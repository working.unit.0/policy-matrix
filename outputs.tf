output "developer_group_arn" {
  value = "${aws_iam_group.developer.arn}"
}

output "powerdev_group_arn" {
  value = "${aws_iam_group.powerdev.arn}"
}

output "june_group_arn" {
  value = "${aws_iam_group.june.arn}"
}

output "tester_group_arn" {
  value = "${aws_iam_group.tester.arn}"
}
