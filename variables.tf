variable "aws_account_id" {
  type = "string"
}

variable "region" {
  type        = "string"
  description = "Aws account region"
}

variable "project_name" {
  type        = "string"
  description = "Templated group and policy naming"
  default     = "example"
}

locals {
  codecommit_name = "${var.project_name}"
}
