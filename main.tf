resource "aws_iam_group" "developer" {
  name = "${var.project_name}-developer"
  path = "/"
}

resource "aws_iam_group_policy_attachment" "CodeCommitDeveloper-attach" {
  group      = "${aws_iam_group.developer.name}"
  policy_arn = "${aws_iam_policy.CodeCommitDeveloper.arn}"
}

resource "aws_iam_group_policy_attachment" "CloudWatchReadOnlyAccess-developer-attach" {
  group      = "${aws_iam_group.developer.name}"
  policy_arn = "arn:aws:iam::aws:policy/CloudWatchReadOnlyAccess"
}

resource "aws_iam_group" "powerdev" {
  name = "${var.project_name}-powerdev"
  path = "/"
}

resource "aws_iam_group_policy_attachment" "CodeCommitPower-power-attach" {
  group      = "${aws_iam_group.powerdev.name}"
  policy_arn = "${aws_iam_policy.CodeCommitPower.arn}"
}

resource "aws_iam_group_policy_attachment" "CloudWatchReadOnlyAccess-powerdev-attach" {
  group      = "${aws_iam_group.powerdev.name}"
  policy_arn = "arn:aws:iam::aws:policy/CloudWatchReadOnlyAccess"
}

resource "aws_iam_group" "june" {
  name = "${var.project_name}-june"
  path = "/"
}

resource "aws_iam_group_policy_attachment" "CodeCommitRO-attach" {
  group      = "${aws_iam_group.june.name}"
  policy_arn = "${aws_iam_policy.CodeCommitRO.arn}"
}

resource "aws_iam_group" "tester" {
  name = "${var.project_name}-tester"
  path = "/"
}

resource "aws_iam_group_policy_attachment" "CloudWatchLogsReadOnlyAccess-tester-attach" {
  group      = "${aws_iam_group.tester.name}"
  policy_arn = "arn:aws:iam::aws:policy/CloudWatchLogsReadOnlyAccess"
}

resource "aws_iam_group" "ops" {
  name = "${var.project_name}-ops"
  path = "/"
}

#######################CloudWatch###################

resource "aws_cloudwatch_log_group" "CloudWatch" {
  name              = "${var.project_name}"
  retention_in_days = "90"
}

#######################Policy#######################

resource "aws_iam_policy" "CodeCommitPower" {
  name        = "${var.project_name}-CodeCommitPower"
  path        = "/"
  description = "Full access to ${var.project_name} git repo"

  policy = "${data.template_file.CodeCommitPower.rendered}"
}

data "template_file" "CodeCommitPower" {
  template = "${file("${path.module}/templates/CodeCommitPower.tpl")}"

  vars {
    project_name   = "${var.project_name}"
    aws_account_id = "${var.aws_account_id}"
    region         = "${var.region}"
  }
}

resource "aws_iam_policy" "CodeCommitDeveloper" {
  name        = "${var.project_name}-CodeCommitDeveloper"
  path        = "/"
  description = "Full access to ${var.project_name} git repo"

  policy = "${data.template_file.CodeCommitDeveloper.rendered}"
}

data "template_file" "CodeCommitDeveloper" {
  template = "${file("${path.module}/templates/CodeCommitDeveloper.tpl")}"

  vars {
    project_name   = "${var.project_name}"
    aws_account_id = "${var.aws_account_id}"
    region         = "${var.region}"
  }
}

resource "aws_iam_policy" "CodeCommitRO" {
  name        = "${var.project_name}-CodeCommitRO"
  path        = "/"
  description = "ReadOnly access to ${var.project_name} git repo"

  policy = "${data.template_file.CodeCommitRO.rendered}"
}

data "template_file" "CodeCommitRO" {
  template = "${file("${path.module}/templates/CodeCommitRO.tpl")}"

  vars {
    project_name   = "${var.project_name}"
    aws_account_id = "${var.aws_account_id}"
    region         = "${var.region}"
  }
}
